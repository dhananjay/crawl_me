package main

import (
//	"time"
)

type counter struct {
	state map[string]bool
	count int
}

// we check the counter (number of links that needs to be
// downloaded), every 2 second. because of the way select works
// if the counter is zero when a tick comes, we stop crawling.
// FIXME: not entirely sure that this doesn't have race conditions
func SetupCounter(newLinks <-chan []string, inchan chan<- string, done chan<- int) {
	c := counter{
		state: make(map[string]bool),
		count: 1, // root is the first link
	}

	//	timer := time.NewTicker(2 * time.Second)

	for {
		select {
		case link := <-newLinks:
			c.count -= 1
			Log.WithField("count", c.count).Debug("new count")
			for _, ln := range link {
				if _, ok := c.state[ln]; !ok {
					c.state[ln] = true
					c.count++
					inchan <- ln
				}
			}

		default:
			// maybe a timer? otherwise we may end up thrashing the checks
			if c.count == 0 {
				Log.Info("crawling finished")
				done <- 0
				return
			}
		}
	}
}
