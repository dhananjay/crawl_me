package main

import (
	"github.com/Sirupsen/logrus"
	"testing"
)

func TestCounter(t *testing.T) {
	newLinks := make(chan []string)
	newDown := make(chan string)
	done := make(chan int)

	Log = logrus.New()

	go SetupCounter(newLinks, newDown, done)

	newLinks <- []string{
		"http://example.com",
	}

	newLink := <-newDown
	if newLink != "http://example.com" {
		t.Error("did n't get expected link")
	}

	// sending same link should stop the counter
	newLinks <- []string{
		"http://example.com",
	}

	<-done

	go SetupCounter(newLinks, newDown, done)

	newLinks <- []string{
		"http://example.com",
	}

	newLink = <-newDown
	if newLink != "http://example.com" {
		t.Error("did n't get expected link")
	}

	// sending a new link, should countinue
	newLinks <- []string{
		"http://example.com",
		"http://example.com/somenew",
	}

	newLink = <-newDown
	if newLink != "http://example.com/somenew" {
		t.Error("did not recive expected link")
	}

	// empty should stop scheduler
	newLinks <- []string{}
	<-done
	// we're cool
}
