package main

import (
	"encoding/json"
	"github.com/boltdb/bolt"
)

func ListLinks(db *bolt.DB, bucket []byte) (links []string) {
	db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket(bucket)

		c := b.Cursor()

		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			links = append(links, string(k))
		}

		return nil
	})
	return
}

func SiteMap(db *bolt.DB, crawlName []byte, root string) []Result {
	data := []Result{}
	visted := []string{}
	dfs(db, root, crawlName, &data, &visted)
	return data
}

// TODO: store this is db to avoid re-computations?
func dfs(db *bolt.DB, root string, bucket []byte, data *[]Result, visted *[]string) {
	getData := func(key []byte) (value []byte, err error) {
		err = db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket(bucket)
			value = b.Get([]byte(key))
			return nil
		})
		return
	}

	v, err := getData([]byte(root))
	if err != nil {
		return
	}
	// expect to be json encoded.
	var value Result
	_ = json.Unmarshal(v, &value)
	*data = append(*data, value)
	*visted = append(*visted, value.Url)

NEXTLINK:
	for _, v := range value.Links {
		for _, nv := range *visted {
			if nv == v {
				continue NEXTLINK
			}
		}

		dfs(db, v, bucket, data, visted)
	}

	return
}
