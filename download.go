package main

import (
	"github.com/mreiferson/go-httpclient"
	"github.com/pkg/errors"
	"github.com/temoto/robotstxt"
	"io/ioutil"
	"net/http"
	"time"
)

type downloader struct {
	c   *http.Client
	rbt *robotstxt.RobotsData
}

func newDownloader(rbtUrl string) *downloader {
	transport := &httpclient.Transport{
		ConnectTimeout:        1 * time.Second,
		RequestTimeout:        10 * time.Second,
		ResponseHeaderTimeout: 5 * time.Second,
	}

	client := &http.Client{Transport: transport}

	// allow all
	rbtCheck, _ := robotstxt.FromString("")

	downObj := &downloader{
		c:   client,
		rbt: rbtCheck,
	}

	req, err := http.NewRequest("GET", rbtUrl, nil)
	if err != nil {
		return downObj
	}

	res, err := client.Do(req)
	if err != nil {
		return downObj
	}

	defer res.Body.Close()

	actualRobot, err := robotstxt.FromResponse(res)
	if err != nil {
		return downObj
	}

	return &downloader{
		c:   client,
		rbt: actualRobot,
	}
}

func (d *downloader) DownloadLink(addr string) ([]byte, int, error) {

	if !d.rbt.TestAgent(addr, "CrawlBot") {
		return nil, 499, errors.New("robots denied")
	}

	req, err := http.NewRequest("GET", addr, nil)
	if err != nil {
		return nil, 0, errors.Wrap(err, "creating request failed")
	}

	res, err := d.c.Do(req)
	if err != nil {
		return nil, 0, errors.Wrap(err, "request failed")
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		// do not read body if non-200
		return nil, res.StatusCode, nil
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, res.StatusCode, errors.Wrap(err, "reading body failed")
	}
	return body, res.StatusCode, nil
}

func downloadWorker(d *downloader, inchan <-chan string, output chan<- DownloadOutput) {
	for {
		// FIXME: stop when inchan is closed.
		select {
		case link := <-inchan:
			Log.WithField("link", link).Info("downloading")
			body, status, err := d.DownloadLink(link)
			output <- DownloadOutput{link, body, status, err}

		}
	}
}

func SetupDownloader(n int, robotsUrl string, inchan <-chan string) <-chan DownloadOutput {
	downloader := newDownloader(robotsUrl)

	output := make(chan DownloadOutput)

	for i := 0; i <= n; i++ {
		go downloadWorker(downloader, inchan, output)
	}

	return output
}
