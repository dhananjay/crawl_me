package main

import (
	"bytes"
	"errors"
	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
	"net/url"
)

func cleanLinks(link, rootDom, rootProto string) (string, error) {
	pu, err := url.Parse(link)
	if err != nil {
		// FIXME: what else can we do?
		return "", errors.New("bad link")
	}

	if pu.Host == "" {
		pu.Host = rootDom
		pu.Scheme = rootProto
	} else if pu.Host != rootDom {
		return "", errors.New("external link")
	}

	return pu.String(), nil
}

func extractData(body []byte, domain, proto string) (links []string) {
	Log.Debug("trying to parse data")
	node, err := html.Parse(bytes.NewReader(body))
	if err != nil {
		Log.Debug("parsing failed")
		// Parsing failed. set empty
		return
	}
	doc := goquery.NewDocumentFromNode(node)

	doc.Find("a").Each(func(i int, sel *goquery.Selection) {
		if res, ok := sel.Attr("href"); ok && res != "#" {
			// check if they are from same domain
			abs, err := cleanLinks(res, domain, proto)
			if err == nil {
				Log.WithField("links", abs).Debug("links")

				links = append(links, abs)
			}
		}
	})

	return
}

func extractWorker(scheme, dom string,
	inchan <-chan DownloadOutput,
	newLinks chan<- []string, storage chan<- Result) {
	for {
		select {
		case data := <-inchan:
			links := []string{}
			Log.WithField("data", data.Address).Debug("recived data to extract")
			if data.Status == 200 && data.Err == nil {
				links = extractData(data.Body, dom, scheme)

				// are they allowed links?

			}

			storage <- Result{
				Status: data.Status,
				Links:  links,
				Url:    data.Address,
			}

			// FIXME: okay, this should have been a data
			// structure, even if we are sending an empty
			// []string, counter uses it to keep track of
			// things.
			newLinks <- links

		}
	}
}

func SetupExtractWorker(n int, proot *url.URL, inchan <-chan DownloadOutput) (<-chan []string, <-chan Result) {
	newLinks := make(chan []string)
	storage := make(chan Result)

	for i := 0; i <= n; i++ {
		go extractWorker(proot.Scheme, proot.Host, inchan, newLinks, storage)
	}

	return newLinks, storage
}
