package main

import (
	"testing"
)

func TestCleanLinks(t *testing.T) {

	_, err := cleanLinks("http://ex2.com/hello", "example.com", "http")
	if err == nil {
		t.Error("external links should be removed")
	}

	cl, err := cleanLinks("/hello", "example.com", "http")
	if err != nil || cl != "http://example.com/hello" {
		t.Error("failed for legit url")
	}
}

func TestExtractData(t *testing.T) {
	// FIXME: this function is not perfect, it misses so much!!

	testHtml := `<html><body><a href="hello">them clones</a></body></html>`
	links := extractData([]byte(testHtml), "example.com", "http")

	if len(links) != 1 {
		t.Error("got unexpected result")
	}
	if links[0] != "http://example.com/hello" {
		t.Error("got :", links)
	}

}
