package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/boltdb/bolt"
	"gopkg.in/alecthomas/kingpin.v2"
	"net/url"
)

var Log *logrus.Logger

func main() {
	var domain = kingpin.Arg("domain", "url to start crawling").
		Required().String()
	var verbose = kingpin.Flag("verbose", "enable verbose logging").
		Short('v').Bool()
	var nocrawl = kingpin.Flag("nocrawl", "query database with past data").
		Short('n').Bool()
	var parallelDownloads = kingpin.Flag("parallel", "no of parallel downloaders (default 10)").
		Short('p').Default("10").Int()

	kingpin.Parse()

	// setup logging
	Log = logrus.New()

	if *verbose {
		Log.Level = logrus.DebugLevel
	} else {
		Log.Level = logrus.WarnLevel
	}

	Log.WithField("answer", 42).Debug("Hell0 fri3nd!")

	db, proot, crawlName := setup(*domain)
	defer db.Close()
	if !*nocrawl {
		crawl(db, proot, crawlName,
			*parallelDownloads)
	}
	query(db, *domain, crawlName)
}

func setup(root string) (*bolt.DB, *url.URL, []byte) {
	// check if root is a proper address
	proot, err := url.Parse(root)
	if err != nil {
		Log.WithError(err).Fatal("bad address to crawl")
	}

	// create a new bolt db to store data
	Log.Debug("opening up database")
	db, err := bolt.Open("crawl.db", 0600, nil)
	if err != nil {
		Log.WithError(err).Fatal("opening database failed!")
	}

	// crawl name - name for this crawl
	crawlName := []byte(proot.Host)

	Log.Debug("creating buckets")
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(crawlName)
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})

	if err != nil {
		Log.WithError(err).Fatal("creating buckets failed")
	}
	Log.Info("setup complete")
	return db, proot, crawlName
}

func crawl(db *bolt.DB, proot *url.URL, crawlName []byte, parllel int) {
	done := make(chan int)
	inchan := make(chan string)

	// Start pipeline here
	// mostly IO bound workers
	downOutput := SetupDownloader(parllel, getRobotUrl(proot), inchan)

	// CPU bound, html parsing ahead
	newLinks, storage := SetupExtractWorker(4, proot, downOutput)

	// worker to dump to storage
	// not concurrent for not complicating storage.
	go StorageWorker(storage, db, crawlName)

	// keeps tracks of links in progress, not concurrent
	go SetupCounter(newLinks, inchan, done)

	inchan <- proot.String()

	// we don't stop all workers, but expect them not to spent much
	// resources as they are blocked on IO
	<-done
	Log.Info("fetching finished, start quering")
}

func query(db *bolt.DB, root string, crawlName []byte) {
	fmt.Println("query for crawl on: ", root)
	for {
		var input string
		fmt.Printf(" %s:> ", string(crawlName))
		fmt.Scanln(&input)

		switch input {
		case "quit":
			fmt.Println("bye!")
			return
		case "list":
			links := ListLinks(db, crawlName)
			for _, v := range links {
				fmt.Println(v)
			}
			fmt.Printf("Stats: %d links\n", len(links))
		case "sitemap":

			for _, v := range SiteMap(db, crawlName, root) {
				fmt.Printf("%v\n", v)
			}

		case "help":
			fmt.Println("commands:\n\tquit, list, sitemap, help")
		default:
			fmt.Println("invalid command!, see help")
		}
	}
}

func getRobotUrl(u *url.URL) string {
	return fmt.Sprintf("%s://%s/robots.txt", u.Scheme, u.Host)
}
