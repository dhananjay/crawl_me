package main

import (
	"net/url"
	"testing"
)

func TestGetRobotsUrl(t *testing.T) {
	urlTestData := [][]string{
		[]string{"http://example.com/somegoof", "http://example.com/robots.txt"},
		[]string{"https://example.com/somegoof", "https://example.com/robots.txt"},
		[]string{"http://booo", "http://booo/robots.txt"},
	}

	for _, urlSet := range urlTestData {
		u, err := url.Parse(urlSet[0])
		if err != nil {
			t.Error("got err while parsing: ", err)
		}
		rburl := getRobotUrl(u)
		if urlSet[1] != rburl {
			t.Error("expected : ", urlSet[1], " to be : ", rburl)
		}
	}

}
