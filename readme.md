# Simple crawler in golang

Crawls a single domain and stores info on a bolt db

## Todo
- [x] robustest
- [x] stopping of worker
- [x] store in bolt and graph
- [x] query and display interface
- [x] arguments passing
- [x] proof
- [ ] test storage and db ops bits
- [x] expose parallelism


## Building
```
go get github.com/Masterminds/glide
glide install
./crawl-me -v -p 10 https://dbalan.in
```

## Usage
query crawl data with
```bash
dbalan.in:> list
http://dbalan.in
http://dbalan.in/cdn-cgi/l/email-protection#620f030b0e220600030e030c4c0b0c
http://dbalan.in/cdn-cgi/l/email-protection#9af7fbf3f6dafef8fbf6fbf4b4f3f4
Stats: 3 links
 dbalan.in:> sitemap
node: http://dbalan.in
Links:
	http://dbalan.in/cdn-cgi/l/email-protection#620f030b0e220600030e030c4c0b0c

Status: 200
node: http://dbalan.in/cdn-cgi/l/email-protection#620f030b0e220600030e030c4c0b0c
Status: 200
```
