package main

import (
	"encoding/json"
	"github.com/boltdb/bolt"
)

func StorageWorker(store <-chan Result, db *bolt.DB, crawlName []byte) {
	for {
		select {
		case data := <-store:
			Log.WithField("data", data.Url).Debug("got storage data")
			res, err := json.Marshal(data)
			if err != nil {
				res = []byte(err.Error())
			}

			err = db.Update(func(tx *bolt.Tx) error {
				b := tx.Bucket(crawlName)
				err := b.Put([]byte(data.Url), res)
				return err
			})
			if err != nil {
				Log.WithError(err).Warn("storing data failed")
			}
		}
	}
}
