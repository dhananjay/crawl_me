package main

import (
	"fmt"
)

type DownloadOutput struct {
	Address string
	Body    []byte
	Status  int
	Err     error
}

type Result struct {
	Url    string   `json:"address"`
	Links  []string `json:"links"`
	Status int      `json:"status_code"`
}

func (r Result) String() string {
	links := ""
	for _, v := range r.Links {
		links += "\t" + fmt.Sprintf("%v\n", v)
	}

	if links != "" {
		return fmt.Sprintf("node: %v\nLinks:\n%s\nStatus: %d", r.Url, links, r.Status)
	} else {
		return fmt.Sprintf("node: %v\nStatus: %d", r.Url, r.Status)
	}
}
